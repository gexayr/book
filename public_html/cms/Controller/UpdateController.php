<?php
/**
 * Created by PhpStorm.
 * User: gex
 * Date: 05.09.17
 * Time: 11:08
 */

namespace Cms\Controller;

use Engine\Core\Template\User;
use Engine\Helper\Cookie;

class UpdateController extends CmsController
{


    public function editUser()
    {
        $params = $this->request->post;
        $files = $this->request->files;
        $id = $params['user-id'];
        $phone = $params['Phone'];


        define('IMG_DIR', ROOT_DIR . "/content/themes/default/img");


        if (preg_match('/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/', $phone)) {

            if (is_uploaded_file($files['avatar']['tmp_name'])) {


                move_uploaded_file($files['avatar']['tmp_name'], IMG_DIR . "/uploads/" . $id . ".jpg");
                $params['img'] = "/content/themes/default/img/uploads/" . $id . ".jpg";

                User::editUser($params);


            } else {

//            $params['img'] = "/content/themes/default/img/uploads/avatar.png";

                User::editUser($params);
            }

        } else {

            print_r('Error Number');
            exit;

        }

    }

    public function settings()
    {
        $params = $this->request->post;

//        echo "<pre>";
//        print_r($params);exit;

        if(isset($params['lang'])){
//            setcookie('language', $params['lang']);
            Cookie::set('language', $params['lang']);
        }

        $params_json = json_encode($params);
//        print_r($params_json);exit;

        if (isset($_COOKIE['auth-Login'])) {
            $login = $_COOKIE['auth-Login'];

            $parameters['user'] = $login;
            $parameters['style_option'] = $params_json;
            User::editUserSettings($parameters);

        } else {
            setcookie('json', $params_json);
        }

//        print_r($parameters);exit;


        header('Location: /');
        exit;
    }

    public function defSettings()
    {
        $params_json = '{"body_bg":"#ffffff","cat_text":"#428bca","book_text":"#333333","cat_bg":"#ffffff","book_bg":"#ffffff","act_cat_text":"#ffffff","cat_text_hover":"#428bca","act_cat_bg":"#448aff","head_bg":"#e1f9ff","head_text":"#777777","head_text_hover":"#333333","cat_opacity":"0.9","book_opacity":"0.9"}';

        if (isset($_COOKIE['auth-Login'])) {
            $login = $_COOKIE['auth-Login'];
        } else {
//            echo "not registered";
            setcookie('json', $params_json);
                    header('Location: /');
                    die();

        }

        $parameters['user'] = $login;
        $parameters['style_option'] = $params_json;
//        print_r($parameters);exit;

        User::editUserSettings($parameters);

//        header('Location: /');
//        exit;
    }


    public function feedBack()
    {
        $params = $this->request->post;

//       print_r($params);

// Check for empty fields
        if (empty($params['email']) ||
            empty($params['message']) ||
            !filter_var($params['email'], FILTER_VALIDATE_EMAIL)
        ) {
            echo "No arguments Provided!";
            return false;
        }

        $email_address = strip_tags(htmlspecialchars($params['email']));
        $message = strip_tags(htmlspecialchars($params['message']));

// Create the email and send the message
        $to = 'gexo7777@gmail.com'; // Add your email address inbetween the '' replacing yourname@yourdomain.com - This is where the form will send a message to.
        $email_subject = "Website Contact Form:  $email_address";
        $email_body = "You have received a new message from your website contact form.\n\n" . "Here are the details:\n\nEmail: $email_address\n\nMessage:\n$message";
        $headers = "From: noreply@gexo777.com\n"; // This is the email address the generated message will be from. We recommend using something like noreply@yourdomain.com.
        $headers .= "Reply-To: $email_address";
        mail($to, $email_subject, $email_body, $headers);

        echo '<script>
            window.alert("Your message was successfully sent.");
            window.location.href="/";
             </script>';
        return true;

    }

    public function PassReset()
    {
        $params = $this->request->post;

//       print_r($params);

// Check for empty fields
        if (empty($params['email']) ||
            !filter_var($params['email'], FILTER_VALIDATE_EMAIL)
        ) {
            echo "No arguments Provided!";
            return false;
        }
        $email_address = strip_tags(htmlspecialchars($params['email']));
        $user = User::getUserData($email_address);
        if($user == null){
            echo "No one with this email is registered";
            return false;
        }
        $link = 'http://'.$_SERVER['HTTP_HOST'];
        $time = time();
        $secret = $email_address . '+' . $time;
        $secret = urlencode($secret);
//        $secret = urldecode($secret);
        $code = $link . '/edit?reset=' . $secret;

//        print_r($code);exit;
// Create the email and send the message
        $to = $email_address;
        $email_subject = "Password Reset Form:  $link";
        $email_body = "Hi.<br>\n\n" . "We got a request to reset your password\n\n<br>"."<br><a href=\"$code\"  style=\"text-decoration: none; background-color: #008bee; color: #fdfbff; padding: 5px 10px 5px 10px; border-radius: 4px; margin: 5px 10px 5px 10px;\">RESET PASSWORD</a><br>\n\n";
        $email_body .= "<br>if you ignore this message, your password won't be changed<br>\n\n";
        $email_body .= "(this link is valid for 30 minutes)\n\n";
        $headers = "From: noreply@yourdomain.com\n";
        $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
        mail($to, $email_subject, $email_body, $headers);


        echo '<script>
            window.alert("We sent you a password reset pass.");
            window.location.href="/";
             </script>';

        return true;
    }


    public function PassUpdate()
    {

        $params = $this->request->post;
        if (isset($params['email']) && isset($params['password']) && $params['email'] != '' && $params['password'] != '') {
            User::editUserPass($params);
        }else{
//            echo "Пароли не совпадают";
            echo '<script>
            window.alert("You did not enter any password");
            window.location.href="/";
             </script>';
            exit;
        }
        return true;

    }


    public function ChangeEmail()
    {

        $params = $this->request->post;

//        print_r($params);exit;
        if (isset($params['email']) && isset($params['old-email']) && $params['email'] != '' && $params['old-email'] != '') {
            $email_address = $params['old-email'];
            $new_email_address = $params['email'];
            $user = User::getUserData($email_address);
            if($user == null){
                echo "No one with this email is registered";
                return false;
            }else{
                $user = User::getUserData($new_email_address);
                if($user == null){
                    User::changeUserData($params);
                }else{
                    echo '<script>
            window.alert("a user with this email is already registered");
            window.location.href="/";
             </script>';
                    exit;
                }
            }
        }else{
//            echo "Пароли не совпадают";
            echo '<script>
            window.alert("You did not enter any email");
            window.location.href="/";
             </script>';
            exit;
        }
        return true;

    }

    public function ChangePass()
    {

        $params = $this->request->post;
//        print_r($params);exit;
        if (isset($params['password']) && isset($params['old-password']) && $params['password'] != '' && $params['old-password'] != '') {

            $email_address = $params['email'];
            $old_password = $params['old-password'];
            $new_password = $params['password'];
            $user = User::getUserData($email_address);
            if($user == null){
                echo "No one with this email is registered";
                return false;
            }else{
//                print_r($user);exit;
                if($user->password == (md5($params['old-password']))){
                    User::changeUserPass($params);
                }else{
                    echo "wrong password";
                }
            }
        }else{
//            echo "Пароли не совпадают";
            echo '<script>
            window.alert("You did not enter any email");
            window.location.href="/";
             </script>';
            exit;
        }
        return true;

    }

}
