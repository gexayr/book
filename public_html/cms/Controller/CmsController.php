<?php
/**
 * Created by PhpStorm.
 * User: gex
 * Date: 17.08.17
 * Time: 12:32
 */

namespace Cms\Controller;


use Engine\Controller;

class CmsController extends Controller
{
    /**
     * HomeController constructor.
     * @param \Engine\DI\DI $di
     */
    public function __construct($di)
    {
        parent::__construct($di);
    }

    public function header()
    {

    }
}