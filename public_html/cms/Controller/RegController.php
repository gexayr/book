<?php

/**
 * Created by PhpStorm.
 * User: gex
 * Date: 16.08.17
 * Time: 18:23
 */

namespace Cms\Controller;


use Engine\Core\Template\User;

class RegController extends CmsController
{


    public function index()
    {
        $this->view->render('index');
    }

    public function news($id)
    {
        echo $id;
    }

    public function add()
    {

        $params       = $this->request->post;

//print_r($params);exit;
        if (isset($params['email']) && isset($params['password']) && $params['email'] != '' && $params['password'] != '') {
            if($params['password'] === $params['confirm-password']){
                User::addUs($params);
            }else{
//            echo "Пароли не совпадают";
                echo '<script>
            window.alert("Passwords are not the same.");
            window.location.href="/";
             </script>';
                exit;
            }
        }else{
//            echo "Пароли не совпадают";
            echo '<script>
            window.alert("You did not enter an email or password");
            window.location.href="/";
             </script>';
            exit;
        }


    }

    public function login()
    {
        $params       = $this->request->post;
//        print_r($params);exit;
        if (isset($params['login']) && isset($params['password']) && $params['login'] != '' && $params['password'] != '') {
            User::logUs($params);
        }else{
//            echo "Пароли не совпадают";
            echo '<script>
            window.alert("You did not enter an email or password");
            window.location.href="/";
             </script>';
            exit;
        }

    }


    public function logout()
    {
        User::logOut();
        header('Location: /');
        exit;
    }

    public function ajaxHtmlInsert()
    {
      $params = $this->request->post;
      User::htmlCat($params);
    }

}
