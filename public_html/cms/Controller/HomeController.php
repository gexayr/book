<?php

/**
 * Created by PhpStorm.
 * User: gex
 * Date: 16.08.17
 * Time: 18:23
 */

namespace Cms\Controller;

use Engine\Core\Template\User;
use Engine\Core\Template\Site;

class HomeController extends CmsController
{

//    public $sites = [];

    public function index()
    {

//        $sites = Site::getSites($page);

        $this->view->render('index');
    }

    public function news($id)
    {
        echo $id;
    }


    public function edit()
    {
        $this->view->render('/edit');
    }


    public function search()
    {

        $params       = $this->request->post;
        $search = $params['search'];

//        <a href="http://yandex.ru/yandsearch?text="></a>
//                <a href="https://www.google.com/search?q="></a>

//        echo '<script>document.window.open("https://www.google.com/search?q='.$search.'", "_blank");</script>';

                header( 'Location: https://www.google.com/search?q='.$search);
    }


    public function profile()
    {
        $this->view->render('profile');
    }




}