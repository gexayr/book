<?php

namespace Cms\Controller;

use Engine\Core\Template\Theme;
use Cms\Model\Bookmarks\BookmarksRepository;
use Engine\Core\Template\Bookmarks;
use Engine\Helper\Cookie;


class BookmarksController extends CmsController
{

    public function bookmarks()
    {

        if (!isset($this->request->get['category_id'])){
          $this->request->get['category_id'] = null;
        }
    $params['categoryId']   = $this->request->get['category_id'];

        $params['cat_def']    = Bookmarks::getCat('1@1.com');
        $params['editBookmarks'] = Bookmarks::getItemsById($params['categoryId']);

        $this->view->render('index', $params);


    }

    public function ajaxBookmarksAdd()
    {
        $params = $this->request->post;

        $this->load->model('Categories', false, 'Cms');

        if (isset($params['name']) && strlen($params['name']) > 0) {
//            $addCategory = $this->model->categories->add($params);
            $addCategory = Bookmarks::addCat($params);

            echo $addCategory;
        }
    }

    public function ajaxBookmarksAddItem()
    {
        $params = $this->request->post;

        if (isset($params['category_id']) && strlen($params['category_id']) > 0) {
            $id = Bookmarks::add($params);

            $item = new \stdClass;
            $item->id   = $id;
            $item->name = $params['name'];
            $item->link = $params['link'];
            $item->images = $params['imageSr'];

            Theme::block('bookmark_item', [
                'item' => $item
            ]);
        }
    }

    public function ajaxBookmarksSortItems()
    {
        $params = $this->request->post;
        //print_r($params);

        $cookie = Cookie::get('auth-Login');
        if ($cookie !== null) {
            if (isset($params['data']) && !empty($params['data'])) {
                $sortItem = Bookmarks::sort($params);
            }
        }
    }

    public function ajaxBookmarksUpdateItem()
    {
        $params = $this->request->post;


        if (isset($params['item_id']) && strlen($params['item_id']) > 0) {
            Bookmarks::update($params);
        }
    }

    public function ajaxBookmarksUpdateCat()
    {
        $params = $this->request->post;

//        print_r($params);

        if (isset($params['cat_id']) && strlen($params['cat_id']) > 0) {
            Bookmarks::updateCat($params);
        }
    }


    public function ajaxBookmarksRemoveItem()
    {
        $params = $this->request->post;

        $itemId = $params['item_id'];
        if (isset($itemId) && strlen($itemId) > 0) {
            $removeItem = Bookmarks::remove($itemId);

            echo $removeItem;
        }
    }

    public function ajaxBookmarksRemoveCat()
    {
        $params = $this->request->post;

        $itemId = $params['item_id'];
        if (isset($itemId) && strlen($itemId) > 0) {
            $removeItem = Bookmarks::removeCat($itemId);

            echo $removeItem;
        }
    }



    public function ajaxCatSortItems()
    {
        $params = $this->request->post;
        $par = $params;
        print_r($par);
        $cookie = Cookie::get('auth-Login');
        if($cookie !== null){
            if (isset($params['data']) && !empty($params['data'])) {
                $sortItem = Bookmarks::sortCat($params);
            }
        }

    }

    public function ajaxCatSortInner()
    {
        $params = $this->request->post;
        $par = $params['data-inner'];
        $par = json_decode($par);
        $par = get_object_vars($par);
        array_shift($par);
//        print_r($par);
        $cookie = Cookie::get('auth-Login');
        if($cookie !== null){
            if (isset($par) && !empty($par)) {
                $sortItem = Bookmarks::sortCatInner($par);
            }
        }

    }

}
