<?php

return [
    'host' => 'localhost',
    'db_name' => 'bookmarks',
    'username' => 'root',
    'password' => 'password',
    'charset' => 'utf8'
];