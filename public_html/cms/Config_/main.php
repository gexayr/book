<?php

return [
    'baseUrl'         => 'http://loc',
    'defaultLang'     => 'english',
    'defaultTimezone' => 'America/Chicago',
    'defaultTheme'    => 'default'
];
