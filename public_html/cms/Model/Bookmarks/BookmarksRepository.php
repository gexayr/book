<?php

/**
 * Created by PhpStorm.
 * User: gex
 * Date: 01.09.17
 * Time: 15:41
 */

namespace Cms\Model\Bookmarks;

use Engine\Model;

class BookmarksRepository extends Model
{

    const NEW_MENU_ITEM_NAME = 'New bookmarks';
    const FIELD_NAME = 'name';
    const FIELD_LINK = 'link';

    public function getAllItems()
    {
        $sql = $this->queryBuilder
            ->select()
            ->from('bookmarks')
            ->orderBy('position', 'ASC')
            ->sql();

        return $this->db->query($sql, $this->queryBuilder->values);
    }



    /**
     * @param $categoryId
     * @param array $params
     * @return mixed
     */
    public function getItems($categoryId, $params = [])
    {
        $sql = $this->queryBuilder
            ->select()
            ->from('bookmarks')
            ->where('category_id', $categoryId)
            ->orderBy('position', 'ASC')
            ->sql();

        return $this->db->query($sql, $this->queryBuilder->values);
    }


    /**
     * @param array $params
     * @return int
     */
    public function add($params = [])
    {
        if(empty($params)){
            return 0;
        }


        $bookmarks = new Bookmarks;

        $bookmarks->setCategoryId($params['category_id']);
        $bookmarks->setName($params['name']);
        $bookmarks->setLink($params['link']);
        $bookmarks->setImages($params['imageSr']);

        $bookmarksId = $bookmarks->save();

        return $bookmarksId;

    }

    /**
     * @param array $params
     * @return int
     */
    public function addDefBook($params = [])
    {
        if(empty($params)){
            return 0;
        }


        $bookmarks = new Bookmarks;

        $bookmarks->setCategoryId($params['category_id']);
        $bookmarks->setName($params['name']);
        $bookmarks->setLink($params['link']);

        $bookmarksId = $bookmarks->save();

        return $bookmarksId;

    }


   /**
    * @param array $params
    */
   public function sort($params = [])
   {
       $items = isset($params['data']) ? json_decode($params['data']) : [];


       if(!empty($items) and isset($items[0])) {
           foreach ($items[0] as $position => $item) {
               $this->db->execute(
                 $this->queryBuilder
                   ->update('bookmarks')
                   ->set(['position' => $position])
                   ->where('id', $item->id)
                   ->sql(),
                 $this->queryBuilder->values
               );
           }
       }
   }


    /**
     * @param array $params
     */
    public function sort2($params = [])
    {
        $items = isset($params['data']) ? json_decode($params['data']) : [];

//                print_r(json_decode($params['data']) );exit;

                $this->db->execute(
                  $this->queryBuilder
                    ->update('bookmarks')
                    ->set(['position' => $params['data']])
                    ->where('id', $value)
                    ->sql(),
                  $this->queryBuilder->values
                );
    }


    /**
     * @param array $params
     * @return int
     */
    public function update($params = [])
    {
        if (empty($params)) {
            return 0;
        }
//print_r($params);exit;
        $bookmarks = new Bookmarks($params['item_id']);
        $bookmarks->setCategoryId($params['cat_id']);
        $bookmarks->setName($params['name']);
        $bookmarks->setLink($params['link']);
        $bookmarks->setImages($params['imageSr']);
        return $bookmarks->save();
    }


    /**
     * @param $itemId
     * @return mixed
     */
    public function remove($itemId)
    {
        $sql = $this->queryBuilder
            ->delete()
            ->from('bookmarks')
            ->where('id', $itemId)
            ->sql();

        return $this->db->query($sql, $this->queryBuilder->values);

    }

    public function getAdminBookList($cat_id)
    {
        if (empty($cat_id)) {
            return 0;
        }
//        $user_id = (int) $user_id;
        $sql = $this->queryBuilder->select('name, link')
            ->from('bookmarks')
            ->where('category_id', $cat_id)
            ->orderBy('id', 'ASC')
            ->sql();

        $query = $this->db->query($sql, $this->queryBuilder->values);
//        return $this->db->query($sql);
        return isset($query[0]) ? $query : null;

    }


}
