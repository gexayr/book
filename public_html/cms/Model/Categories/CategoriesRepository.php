<?php


namespace Cms\Model\Categories;

use Engine\Model;
use Engine\Core\Database\QueryBuilder;


class CategoriesRepository extends Model
{
    /**
     * @param array $params
     * @return int
     */
    public function add($params = [])
    {
        if(empty($params)) {
            return 0;
        }

//        print_r($params);exit;
        $category = new Categories;
        $category->setName($params['name']);
        $category->setUserLogin($params['login']);
        $categoryId = $category->save();

        return $categoryId;
    }


    /**
     * @param array $params
     * @return int
     */
    public function update($params = [])
    {
        if (empty($params)) {
            return 0;
        }

        $category = new Categories($params['cat_id']);

//        if ($params['field'] == self::FIELD_NAME) {
            $category->setName($params['value']);
//        }
//        if ($params['field'] == self::FIELD_LINK) {
//            $category->setLink($params['value']);
//        }

        return $category->save();
    }



    public function remove($itemId)
    {
//        print_r($itemId);exit;
        $sql = $this->queryBuilder
            ->delete()
            ->from('categories')
            ->where('id', $itemId)
            ->sql();

        return $this->db->query($sql, $this->queryBuilder->values);
    }


    public function getIdCat($user_login)
    {
//        $user_id = (int) $user_id;
        $sql = $this->queryBuilder->select('id')
            ->from('categories')
            ->where('user_login', $user_login)
            ->orderBy('id', 'DESC')
            ->limit(1)
            ->sql();

        $query = $this->db->query($sql, $this->queryBuilder->values);
//        return $this->db->query($sql);
        return isset($query[0]) ? $query[0]->id : null;

    }

    public function getCatList($user_login)
    {
//        $user_id = (int) $user_id;
        $sql = $this->queryBuilder->select()
            ->from('categories')
            ->where('user_login', $user_login)
            ->orderBy('position', 'ASC')
            ->sql();

        $query = $this->db->query($sql, $this->queryBuilder->values);
//        return $this->db->query($sql);
        return isset($query[0]) ? $query : null;

    }

//
//    public function getCatListId($user_login)
//    {
////        $user_id = (int) $user_id;
//        $sql = $this->queryBuilder->select('id')
//            ->from('categories')
//            ->where('user_login', $user_login)
//            ->orderBy('id', 'ASC')
//            ->sql();
//
//        $query = $this->db->query($sql, $this->queryBuilder->values);
////        return $this->db->query($sql);
//        return isset($query[0]) ? $query : null;
//
//    }

    public function getAdminCatList($field)
    {
//        $user_id = (int) $user_id;
        $sql = $this->queryBuilder->select($field)
            ->from('categories')
            ->where('user_login', '1@1.com')
            ->orderBy('id', 'ASC')
            ->sql();

//        print_r($sql);exit;
        $query = $this->db->query($sql, $this->queryBuilder->values);
//        return $this->db->query($sql);
        return isset($query[0]) ? $query : null;

    }

//
//    /**
//     * @param array $params
//     */
//    public function sort($params = [])
//    {
//        $items = isset($params['data']) ? json_decode($params['data']) : [];
//
//        if(!empty($items) and isset($items[0])) {
//            foreach ($items[0] as $position => $item) {
//                $this->db->execute(
//                    $this->queryBuilder
//                        ->update('categories')
//                        ->set(['position' => $position])
//                        ->where('id', $item->id)
//                        ->sql(),
//                    $this->queryBuilder->values
//                );
//            }
//        }
//    }
//


    /**
     * @param array $params
     */
    public function sort($params = [])
    {
        $items = isset($params['data']) ? json_decode($params['data']) : [];

        if(!empty($items) and isset($items)) {
            foreach ($items as $key => $value) {
                $this->db->execute(
                    $this->queryBuilder
                        ->update('categories')
                        ->set(['position' => $key])
                        ->where('id', $value)
                        ->sql(),
                    $this->queryBuilder->values
                );
            }
        }
    }

    /**
     * @param array $params
     */
    public function sortInner($params = [])
    {
        $items = isset($params) ? ($params) : [];
//        print_r($items);

        if(!empty($items) and isset($items)) {
            foreach ($items as $key => $value) {



                $parent_id = explode('-',$key);
                if(!isset($parent_id[1])){
                    $parent_id = "-";
//                    print_r($value);

                    foreach ($value as $key => $value) {
                        $this->db->execute(
                            $this->queryBuilder
                                ->update('categories')
                                ->set(['position' => $key, 'parent_id' =>$parent_id])
                                ->where('id', $value)
                                ->sql(),
                            $this->queryBuilder->values
                        );
                    }
        }else{
                    $parent_id = $parent_id[1];
            foreach ($value as $key => $value) {
                $this->db->execute(
                    $this->queryBuilder
                        ->update('categories')
                        ->set(['position' => $key, 'parent_id' =>$parent_id])
                        ->where('id', $value)
                        ->sql(),
                    $this->queryBuilder->values
                );
            }
                }

//                $item[] = $value;
            }
        }
    }

public function changeEmail($params)
   {
       if (empty($params)) {
           return 0;
       }

       $this->db->execute(
           $this->queryBuilder
               ->update('categories')
               ->set(['user_login' => $params['email']])
               ->where('user_login', $params['old-email'])
               ->sql(),
           $this->queryBuilder->values
       );
   }

}
