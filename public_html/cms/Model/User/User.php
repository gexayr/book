<?php

namespace Cms\Model\User;

use Engine\Core\Database\ActiveRecord;

class User
{
    use ActiveRecord;

    /**
     * @var string
     */
    protected $table = 'user';

    /**
     * @var User id
     */
    public $id;

    /**
     * @var User email
     */
    public $email;

    /**
     * @var User password
     */
    public $password;


    public $date_reg;

    public $style_option;

    /**
     * @return mixed
     */
    public function getStyleOption()
    {
        return $this->style_option;
    }

    /**
     * @param mixed $style_option
     */
    public function setStyleOption($style_option)
    {
        $this->style_option = $style_option;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }


    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param User $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param User $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }


    /**
     * @return mixed
     */
    public function getDateReg()
    {
        return $this->date_reg;
    }

    /**
     * @param mixed $date_reg
     */
    public function setDateReg($date_reg)
    {
        $this->date_reg = $date_reg;
    }



}