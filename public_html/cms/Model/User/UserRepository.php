<?php

namespace Cms\Model\User;

use Engine\Helper\Cookie;
use Engine\Model;
use Engine\Core\Database\QueryBuilder;

class UserRepository extends Model
{
    public function getUsers()
    {
        $sql = $this->queryBuilder->select()
            ->from('user')
            ->orderBy('id', 'DESC')
            ->sql();

        return $this->db->query($sql);
    }

    public function getAllUsersLogin()
    {
        $sql = $this->queryBuilder->select()
            ->from('user')
            ->orderBy('id', 'DESC')
            ->sql();

        $results = $this->db->query($sql);

        foreach ($results as $result) {
            $result_arr[$result->id] = $result->email;
        }
        return $result_arr;
    }

    public function getUsersSocId()
    {
        $sql = $this->queryBuilder->select()
            ->from('user')
            ->orderBy('id', 'DESC')
            ->sql();

        $results = $this->db->query($sql);

        foreach ($results as $result) {
            $result_arr[$result->id] = $result->soc_akk_id;
        }
        return $result_arr;
    }


    public function getUserData($id)
    {
//        print_r($id);exit;
        $user = new User($id);


        return $user->findOne();
//        print_r($project->findOne());exit;
    }

    public function editUserSettings($params)
    {
        if(empty($params)) {
            return 0;
        }
//        print_r($params);exit;
        $login =  $params['user'];
        $style_option =  $params['style_option'];

        $queryBuilder = new QueryBuilder();

        $sql = $queryBuilder
            ->update('user')
            ->set(['style_option' => $style_option])
            ->where('email', $login)->sql();

        $this->db->execute($sql, $queryBuilder->values);


        echo '<script>
            window.alert("Settings successfully saved.");
            window.location.href="/";
             </script>';

    }


    public function getUserByLogin($login)
    {
        $sql = $this->queryBuilder->select()
            ->from('user')
            ->where('email', $login)
            ->limit(1)
            ->sql();
//        return $sql;

        $query = $this->db->query($sql, $this->queryBuilder->values);

        return isset($query[0]) ? $query[0] : null;
    }


    public function addUser($params)
    {

    }


    public function add($params)
    {

        Cookie::set('auth-Login', $params['email']);

        $user = new User;
        $user->setEmail($params['email']);
        $user->setPassword(md5($params['password']));
        $userId = $user->save();
        header('Location: /');
        return $userId;

    }

    public function getUsersLogin()
    {
        $sql = $this->queryBuilder->select()
            ->from('user')
            ->orderBy('id', 'DESC')
            ->sql();

        $results = $this->db->query($sql);

        foreach ($results as $result) {
            $result_arr[$result->id] = $result->login;
        }
        return $result_arr;
    }

    public function getUsersAuth($params)
    {
//        $params       = $this->request->post;
        $queryBuilder = new QueryBuilder();

        $sql = $queryBuilder
            ->select()
            ->from('user')
            ->where('email', $params['login'])
            ->where('password', md5($params['password']))
            ->limit(1)
            ->sql();

        $query = $this->db->query($sql, $queryBuilder->values);
//         print_r($params);exit;

        if (!empty($query)) {
            $user = $query[0];
            $userId = $user->id;
            $this->db->execute($sql, $queryBuilder->values);
            Cookie::set('auth-Login', $params['login']);
            header( 'Location: /');
            exit;
        }

//        echo 'Incorrect Login or password.';
        echo '<script>
            window.alert("Incorrect Login or password.");
            window.location.href="/";
             </script>';
    }

    public function addHtmlCat($params)
    {
      $queryBuilder = new QueryBuilder();

      $sql = $queryBuilder
          ->update('user')
          ->set(['html_data' => $params['data-inner']])
          ->where('email', $params['usId'])->sql();

      $this->db->execute($sql, $queryBuilder->values);

    }

    public function getHtmlCat($params)
    {
      $queryBuilder = new QueryBuilder();

      $sql = $this->queryBuilder->select('html_data')
          ->from('user')
          ->where('email', "a@a")
          ->sql();

//        return $this->db->query($sql);

      return $this->db->query($sql, $this->queryBuilder->values);

    }



    public function editUserPass($params)
    {
//        print_r($params);exit;
        if(empty($params)) {
            return 0;
        }
        $login =  $params['email'];
        $pass =  (md5($params['password']));
//        print_r($pass);exit;

        $queryBuilder = new QueryBuilder();

        $sql = $queryBuilder
            ->update('user')
            ->set(['password' => $pass])
            ->where('email', $login)->sql();

        $this->db->execute($sql, $queryBuilder->values);


        echo '<script>
            window.alert("YOUR PASSWORD CHANGED.");
            window.location.href="/";
             </script>';

    }


    public function changeUserData($params)
    {
//        print_r($params);exit;
        if(empty($params)) {
            return 0;
        }
        $login =  $params['old-email'];
        $new_login =  $params['email'];
//        print_r($pass);exit;

        $queryBuilder = new QueryBuilder();

        $sql = $queryBuilder
            ->update('user')
            ->set(['email' => $new_login])
            ->where('email', $login)->sql();

        $this->db->execute($sql, $queryBuilder->values);

//        Cookie::set('auth-Login', $params['email']);
     echo '<script>
            window.alert("YOUR EMAIL CHANGED.");
            window.location.href="/";
             </script>';
      //  header( 'Location: /');
        exit;

    }


    public function changeUserPass($params)
    {
        if(empty($params)) {
            return 0;
        }
        $password =  (md5($params['old-password']));
        $new_password =  (md5($params['password']));
//        print_r($pass);exit;

        $queryBuilder = new QueryBuilder();

        $sql = $queryBuilder
            ->update('user')
            ->set(['password' => $new_password])
            ->where('password', $password)->sql();

        $this->db->execute($sql, $queryBuilder->values);

//        Cookie::set('auth-Login', $params['email']);
        header( 'Location: /');
        exit;

    }


    public function getUsersCheck()
    {
        $sql = $this->queryBuilder->select('password')
            ->from('user')
            ->orderBy('id', 'DESC')
            ->sql();

//        return $this->db->query($sql);

        $results = $this->db->query($sql);

        foreach ($results as $result) {
            $result_arr[] = $result->password;
        }
        return $result_arr;
    }


}
