<?php
/**
 * List routes
 */


$this->router->add('home', '/', 'BookmarksController:bookmarks');
$this->router->add('reg-user', '/reg/add', 'RegController:add', 'POST');
$this->router->add('login', '/login', 'HomeController:login');
$this->router->add('login-auth', '/login/auth', 'RegController:login', 'POST');
$this->router->add('logout', '/logout', 'RegController:logout');
$this->router->add('search', '/search', 'HomeController:search', 'POST');
$this->router->add('pass-edit', '/edit', 'HomeController:edit');
$this->router->add('profile', '/profile', 'HomeController:profile');
$this->router->add('user-settings', '/settings', 'UpdateController:settings', 'POST');
$this->router->add('user-def-settings', '/default-settings', 'UpdateController:defSettings');
$this->router->add('feed-back', '/feed-back', 'UpdateController:feedBack', 'POST');
$this->router->add('pass-reset', '/pass/reset', 'UpdateController:PassReset', 'POST');
$this->router->add('pass-update', '/update/pass', 'UpdateController:PassUpdate', 'POST');
$this->router->add('change-email', '/change/email', 'UpdateController:ChangeEmail', 'POST');
$this->router->add('change-pass', '/change/pass', 'UpdateController:ChangePass', 'POST');




$this->router->add('bookmarks-bookmarks', '/bookmarks/', 'BookmarksController:bookmarks');

$this->router->add('bookmark-add-bookmarks', '/ajaxBookmarksAdd/', 'BookmarksController:ajaxBookmarksAdd', 'POST');
$this->router->add('bookmark-add-bookmarks-item', '/ajaxBookmarksAddItem/', 'BookmarksController:ajaxBookmarksAddItem', 'POST');
$this->router->add('bookmark-sort-bookmarks-item', '/ajaxBookmarksSortItems/', 'BookmarksController:ajaxBookmarksSortItems', 'POST');
$this->router->add('bookmark-remove-bookmarks-item', '/ajaxBookmarksRemoveItem/', 'BookmarksController:ajaxBookmarksRemoveItem', 'POST');
$this->router->add('bookmark-remove-cat-item', '/ajaxBookmarksRemoveCat/', 'BookmarksController:ajaxBookmarksRemoveCat', 'POST');
$this->router->add('bookmark-update-bookmarks-item', '/ajaxBookmarksUpdateItem/', 'BookmarksController:ajaxBookmarksUpdateItem', 'POST');

$this->router->add('bookmark-update-cat-item', '/ajaxBookmarksUpdateCat/', 'BookmarksController:ajaxBookmarksUpdateCat', 'POST');

//=test
$this->router->add('cat-sort-bookmarks-item', '/ajaxCatSortItems/', 'BookmarksController:ajaxCatSortItems', 'POST');
$this->router->add('cat-sort-bookmarks-inner', '/ajaxCatSortInner/', 'BookmarksController:ajaxCatSortInner', 'POST');
$this->router->add('cat-sort-bookmarks-html', '/ajaxHtmlInsert/', 'RegController:ajaxHtmlInsert', 'POST');
