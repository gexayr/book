<?php
/**
 * Created by PhpStorm.
 * User: gex
 * Date: 24.08.17
 * Time: 17:32
 */

namespace Engine\Core\Template;

use Cms\Model\User\UserRepository;
use Cms\Model\Categories\CategoriesRepository;
use Cms\Model\Bookmarks\BookmarksRepository;


use Engine\DI\DI;
use Engine\Helper\Cookie;

/**
 * Class User
 * @package Engine\Core\Template
 */
class User
{
    /**
     * @var
     */
    protected static $di;

    /**
     * @var UserRepository
     */
    protected static $userRepository;
    protected static $bookmarksRepository;
    protected static $categoriesRepository;


    public function __construct($di)
    {

        self::$di = $di;
        self::$userRepository = new UserRepository(self::$di);
        self::$bookmarksRepository = new BookmarksRepository(self::$di);
        self::$categoriesRepository = new CategoriesRepository(self::$di);

    }

    /**
     * @param string
     * @return null|string
     */




    public static function getCookie($key)
    {
//        $login

        if (isset($_COOKIE[$key])) {
            return $_COOKIE[$key];
        }else{
            return null;
        }

    }


    public static function getUser()
    {
        $login = Cookie::get('auth-Login');

        return $login;

    }


    public static function getUserData($login)
    {

            return self::$userRepository->getUserByLogin($login);

    }

    public static function testData()
    {
//        $field = 'name';
        $field = 'id';
        $default_cat = self::$categoriesRepository->getAdminCatList($field);
        foreach ($default_cat as $defaultCatParams) {
            $defaultParamsArr[] = $defaultCatParams->$field;

        }

            return $defaultParamsArr;

    }

    public static function testData1()
    {


        $field = 'id';
        $default_cat = self::$categoriesRepository->getAdminCatList($field);
        foreach ($default_cat as $defaultCatParams) {
            $defaultParamsArr[] = $defaultCatParams->$field;

        }
        foreach ($defaultParamsArr as $deframs) {

            $admin_book[] = self::$bookmarksRepository->getAdminBookList($deframs);
        }

        return $admin_book;
    }


    public static function getUserById($id)
    {

        return self::$userRepository->getUserData($id);

    }


    public static function addUs($params)
    {
        $users_login = self::$userRepository->getAllUsersLogin();
//        print_r($users_login);exit;
        if (in_array($params['email'], $users_login)) {

//            echo "Пользователь с таким логином уже существует";
            echo '<script>
            window.alert("A user with such login already exists.");
            window.location.href="/";
             </script>';
            die();
        } else {

//            $field = 'name';
//            $admin_cat = self::$categoriesRepository->getAdminCatList($field);
//            foreach ($admin_cat as $adminCatParams) {
//                $default_cat[] = $adminCatParams->name;
//            }

//            $field = 'id';
//            $admin_cat_id = self::$categoriesRepository->getAdminCatList($field);
//            foreach ($admin_cat as $adminCatParams) {
//                $default_cat_id[] = $adminCatParams->id;
//            }

//            e_%27Hy8k

//            foreach ($default_cat_id as $admin_cat_id) {
//
//                $admin_book[] = self::$bookmarksRepository->getAdminBookList($admin_cat_id);
//
//            }

            $default_cat = array('Popular',
                'Cinema',
                'Social networks',
                'Music',
                'Travels',
                'Books',
                'Sport',
                'Purchases'
            );


            $cat_books = array(
                array(
                    'Google' => 'google.com',
                    'Yandex' => 'yandex.ru',
                    'Mail' => 'mail.ru',
                    'Youtube' => 'Youtube.com',
                    'FaceBook' => 'fb.com'

                ),
                array(
                    'kinopoisk.ru - Top 250' => 'kinopoisk.ru',
                    'IMDb Top 250' => 'imdb.com'
                ),
                array(
                    'Facebook' => 'facebook.com',
                    'Google+' => 'plus.google.com',
                    'VK' => 'vk.com'
                ),

                array(
                    'Radio.Yandex' => 'https://radio.yandex.ru',
                    'Music.Yandex' => 'https://music.yandex.ua/'
                ),

                array(
                    'BOOK.travel' => 'https://www.book.travel/'
                ),

                array(
                    'Google Books' => 'https://books.google.ru/'
                ),

                array(
                    'Football' => 'fifa.com',
                    'Basketball' => 'nba.com'
                ),

                array(
                    'Amazon' => 'amazon.com',
                    'eBay' => 'ebay.com'
                ),
            );

            $i = 0;
            foreach ($default_cat as $defaultCatParams) {
                $defaultParamsArr['name'] = $defaultCatParams;
                $defaultParamsArr['login'] = $params['email'];

                self::$categoriesRepository->add($defaultParamsArr);
                $cat_id = self::$categoriesRepository->getIdCat($params['email']);

//                print_r($cat_id);exit;

                $book_params['category_id'] = $cat_id;
                $default_book = $cat_books[$i];

            foreach ($default_book as $key=>$value) {
                $book_params['name'] = $key;
                $book_params['link'] = $value;

                    self::$bookmarksRepository->addDefBook($book_params);
                }
$i++;
            }
            return self::$userRepository->add($params);
        }
    }


    public static function logUs($params)
    {
//print_r($params);
        return self::$userRepository->getUsersAuth($params);

    }

    public static function htmlCat($params)
    {
      return self::$userRepository->addHtmlCat($params);
    }

    public static function gethtmlCat($params)
    {
      return self::$userRepository->getHtmlCat($params);
    }

    public static function editUserSettings($params)
    {

        //        $login = Cookie::get('auth-Login');

        return self::$userRepository->editUserSettings($params);

    }

    public static function logOut()
    {
//print_r('gag');exit;
        return Cookie::delete('auth-Login');

    }

    public static function editUserPass($params)
    {

        return self::$userRepository->editUserPass($params);

    }

    public static function changeUserData($params)
    {


//        Cookie::delete('auth-Login');
//        Cookie::set('auth-Login', $params['email']);
        self::$categoriesRepository->changeEmail($params);
        return self::$userRepository->changeUserData($params);

    }

    public static function changeUserPass($params)
    {

        return self::$userRepository->changeUserPass($params);

    }

}
