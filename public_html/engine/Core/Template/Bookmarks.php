<?php
/**
 * Created by PhpStorm.
 * User: gex
 * Date: 25.08.17
 * Time: 10:03
 */

namespace Engine\Core\Template;

use Engine\DI\DI;
use Cms\Model\Bookmarks\BookmarksRepository;
use Cms\Model\Categories\CategoriesRepository;

class Bookmarks
{

    /**
     * @var Di
     */
    protected static $di;

    /**
     * @var BookmarksRepository
     */
    protected static $bookmarksRepository;
    protected static $categoriesRepository;


    public function __construct($di)
    {
        self::$di = $di;
        self::$bookmarksRepository = new BookmarksRepository(self::$di);
        self::$categoriesRepository = new CategoriesRepository(self::$di);
    }

    public static function show()
    {

    }

    public static function getItems()
    {
        return self::$bookmarksRepository->getAllItems();
    }
//
//    public static function getMenuItems($menuId)
//    {
//        return self::$bookmarksRepository->getItems($menuId);
//    }

    public static function getCat($user_login)
    {
        return self::$categoriesRepository->getCatList($user_login);
    }
//
//
//    public static function getCatId($user_login)
//    {
//        $cat_id = self::$categoriesRepository->getCatListId($user_login);
//        foreach ($cat_id as $cat_is){
//            $cat_isd[] = $cat_is->id;
//        }
//        return $cat_isd;
//    }


    public static function getItemsById($id)
    {
        return self::$bookmarksRepository->getItems($id);
    }

    public static function add($params)
    {
        return self::$bookmarksRepository->add($params);
    }
    public static function addCat($params)
    {
        return self::$categoriesRepository->add($params);
    }
    public static function sort($params)
    {
        return self::$bookmarksRepository->sort($params);
    }
    public static function sortCat($params)
    {
        return self::$categoriesRepository->sort($params);
    }
    public static function sortCatInner($params)
    {
        return self::$categoriesRepository->sortInner($params);
    }

    public static function update($params)
    {
        return self::$bookmarksRepository->update($params);
    }


    public static function updateCat($params)
    {
        return self::$categoriesRepository->update($params);
    }

    public static function remove($itemId)
    {
        return self::$bookmarksRepository->remove($itemId);
    }

    public static function removeCat($itemId)
    {
        return self::$categoriesRepository->remove($itemId);
    }
}
