// $(function() {
//     var group = $("ol.edit-menu").sortable({
//         group: 'edit-menu',
//         handle: '.book-items',
//         onDragStart: function ($item, container, _super) {
//             // Duplicate items of the no drop area
//             if(!container.options.drop)
//                 $item.clone().insertAfter($item);
//             _super($item, container);
//         },
//         onDrop: function ($item, container, _super) {
//             var data = group.sortable("serialize").get();
//             var jsonString = JSON.stringify(data, null, ' ');
//             var formData = new FormData();
//
//             //console.log(data);
//
//             formData.append('data', jsonString);
//             formData.append('menu_id', $('#sortMenuId').val());
//
//             $.ajax({
//                 url: '/ajaxBookmarksSortItems/',
//                 type: 'POST',
//                 data: formData,
//                 processData: false,
//                 contentType: false,
//                 beforeSend: function(){
//
//                 },
//                 success: function(result){
//                     console.log(result);
//                 }
//             });
//
//             _super($item, container);
//         }
//     });
//
//
//     var cat = $("ul.cat-list-ul").sortable({
//         group: 'cat-list-ul',
//         handle: '.list-group',
//         onDragStart: function ($item, container, _super) {
//             // Duplicate items of the no drop area
//             if(!container.options.drop)
//                 $item.clone().insertAfter($item);
//             _super($item, container);
//         },
//         onDrop: function ($item, container, _super) {
//             var data = cat.sortable("serialize").get();
//             var jsonString = JSON.stringify(data, null, ' ');
//             var formData = new FormData();
//
//             //console.log(data);
//
//             formData.append('data', jsonString);
//             // formData.append('cat_id', $('#sortCatId').val());
//
//             $.ajax({
//                 url: '/ajaxCatSortItems/',
//                 type: 'POST',
//                 data: formData,
//                 processData: false,
//                 contentType: false,
//                 beforeSend: function(){
//
//                 },
//                 success: function(result){
//                     // console.log(result);
//                 }
//             });
//
//             _super($item, container);
//         }
//     });
//
// });