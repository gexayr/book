var user = {
    ajaxMethod: 'POST',

    add: function() {
        var formData = new FormData();

        formData.append('login', $('#login').val());
        formData.append('email', $('#email').val());
        formData.append('password', $('#password').val());


        $.ajax({
            url: '/reg/add/',
            type: this.ajaxMethod,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function(){

            },
            success: function(result){
                console.log(result);
                // window.location = '/admin/posts/edit/' + result;
            }
        });
    }

    // update: function() {
    //     var formData = new FormData();
    //
    //     formData.append('post_id', $('#formPostId').val());
    //     formData.append('title', $('#formTitle').val());
    //     formData.append('content', $('.redactor-editor').html());
    //
    //     $.ajax({
    //         url: '/admin/post/update/',
    //         type: this.ajaxMethod,
    //         data: formData,
    //         cache: false,
    //         processData: false,
    //         contentType: false,
    //         beforeSend: function(){
    //
    //         },
    //         success: function(result){
    //             console.log(result);
    //         }
    //     });
    // }
};