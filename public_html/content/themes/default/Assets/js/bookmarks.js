var bookmark = {
    listItems: $('#listItems'),

    ajaxMethod: 'POST',

    add: function() {
        var formData = new FormData();
        var CatName = $('#catName').val();
        var UsLog = $('#usLog').val();

        formData.append('name', CatName);
        formData.append('login', UsLog);

        if (CatName.length < 1) {
            return false;
        }

        $.ajax({
            url: '/ajaxBookmarksAdd/',
            type: this.ajaxMethod,
            data: formData,
            processData: false,
            contentType: false,
            beforeSend: function(){

            },
            success: function(result){
                console.log(result);
                if (result > 0) {
                    // console.log(result);
                    location.reload();

                }


            }

        });

    },

    addItem: function() {
        var formData = new FormData();
        var BookName = $('#BookName').val();
        var BookLink = $('#BookLink').val();
        var categoryId = $('#SelectCategory').val();
        var imgSrc = document.getElementById('let').value;
        formData.append('name', BookName);
        formData.append('link', BookLink);
        formData.append('category_id', categoryId);
        formData.append('imageSr', imgSrc);

        if (categoryId < 1) {
            return false;
        }
        var _this = this;
        $.ajax({
            url: '/ajaxBookmarksAddItem/',
            type: this.ajaxMethod,
            data: formData,
            processData: false,
            contentType: false,
            beforeSend: function(){

            },
            success: function(result){
                console.log(result);
                if (result) {
                    _this.listItems.append(result);
                }
                window.location.href="/?category_id="+categoryId;
            }
        });
    },

    updateItem: function(itemId) {

        var formData = new FormData();

        formData.append('item_id', itemId);
        formData.append('name', $('#name-book-'+itemId).val());
        formData.append('link', $('#link-book-'+itemId).val());
        formData.append('cat_id', $('#SelectCategory-'+itemId).val());
        formData.append('imageSr', document.getElementById('let' + itemId).value)
        console.log(formData);

        if (itemId < 1) {
            return false;
        }

        var _this = this;
        $.ajax({
            url: '/ajaxBookmarksUpdateItem/',
            type: this.ajaxMethod,
            data: formData,
            processData: false,
            contentType: false,
            beforeSend: function(){

            },
            success: function(result){
                console.log(result);
                $('#book-item-name-' + itemId).html( $('#name-book-'+itemId).val() );
                $('#book-item-link-' + itemId).attr("href", "//"+$('#link-book-'+itemId).val() );
                window.location.href="/?category_id="+$('#cat-id-by-item-'+itemId).val();
                if (result) {
                }
            }
        });
    },

    updateCat: function(itemId) {

        var formData = new FormData();

        formData.append('cat_id', itemId);
        formData.append('value', $('#name-cat-'+itemId).val());

        console.log(formData);

        if (itemId < 1) {
            return false;
        }

        var _this = this;
        $.ajax({
            url: '/ajaxBookmarksUpdateCat/',
            type: this.ajaxMethod,
            data: formData,
            processData: false,
            contentType: false,
            beforeSend: function(){

            },
            success: function(result){
                console.log(result);
                $('#cat-item-a-' + itemId).html( $('#name-cat-'+itemId).val() )


                if (result) {

                }
                window.location.href="/?category_id="+itemId;

            }
        });
    },

    removeItem: function(itemId) {

        if(!confirm('Delete the Bookmarks item?')) {
            return false;
        }

        var formData = new FormData();

        formData.append('item_id', itemId);

        if (itemId < 1) {
            return false;
        }

        $.ajax({
            url: '/ajaxBookmarksRemoveItem/',
            type: this.ajaxMethod,
            data: formData,
            processData: false,
            contentType: false,
            beforeSend: function(){

            },
            success: function(result){
                $('.book-item-' + itemId).remove();
            }
        });
    },


    removeCat: function(itemId) {

        if(!confirm('Delete the Category?')) {
            return false;
        }

        var formData = new FormData();

        formData.append('item_id', itemId);

        if (itemId < 1) {
            return false;
        }

        $.ajax({
            url: '/ajaxBookmarksRemoveCat/',
            type: this.ajaxMethod,
            data: formData,
            processData: false,
            contentType: false,
            beforeSend: function(){

            },
            success: function(result){
                console.log(result);
                $('.cat-item-' + itemId).remove();
            }
        });
    },

    mustReg: function() {
        window.alert("You must register in order to use these options.");
        return false;
    },


    LastCat: function() {
        window.alert("It is necessary to leave at least one category.");
        return false;
    }

};
