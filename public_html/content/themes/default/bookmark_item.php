<?php
$login = User::getCookie('auth-Login');
$categories = Bookmarks::getCat($login);
$dirimg = ROOT_DIR.'/content/themes/default/img/book';
$filesimage = scandir($dirimg);
array_shift($filesimage);
array_shift($filesimage);
foreach($filesimage as $value){
    $pieces = explode("+", $value);
//    print_r(explode(".", $value));exit;
    $arr[] = $pieces[0];
}
//echo "<pre>";
//print_r($arr);


?>
<li class="book-items book-item-<?= $item->id ?>" data-id="<?= $item->id ?>">
    <?php
    $url = $item->link;
    $vowels = array("http://", "https://", "www.");
    $url = str_replace($vowels, "", $url);
//    $link = explode(".", $url);
//    $link = $link[0];
//print_r($url);
    ?>
    <div class="book-name">


        <div class="menu-item-event menu-item-event-remove">
                <? if($login == null) {?>
                    <button class="button-remove" onclick="bookmark.mustReg()">
                        <i class="icon-trash icons"></i>
                    </button>
                <? }else{ ?>
                    <button class="button-remove" onclick="bookmark.removeItem(<?= $item->id ?>)">
                        <i class="icon-trash icons"></i>
                    </button>
                <? } ?>
        </div>

        <div class="menu-item-event menu-item-event-edit">
            <? if($login == null) {?>
                <a class="myBook" href="javascript:void(0)" onclick="bookmark.mustReg()">
                    <i class="icon-pencil icons"></i>
                </a>
            <? }else{ ?>
                <a data-toggle="modal" class="myBook" href="#myBookName<?= $item->id ?>">
                    <i class="icon-pencil icons"></i>
                </a>
            <? } ?>
        </div>
    </div>
    <div class="bookmark-link">

        <a href="//<?= $url ?>" target="_blank" id="book-item-link-<?= $item->id ?>">
            <?php
            if ($item->images != "NULL" && strlen($item->images) > 5) {
              echo '<img src="'.$item->images.'" alt="">';
            }else {
              if(in_array($url, $arr)){
                foreach ($arr as $value){
                    if($url == $value){
                        echo '<img src="/content/themes/default/img/book/'.$url.'+.png" alt="">';
                    }
                }
            }else{ ?>
            <img src="http://www.google.com/s2/favicons?domain=<?=$url?>" alt="" style="top: 50%">
          <?php }} ?>
        </a>
    </div>
    <div class="itam-name" id="book-item-name-<?= $item->id ?>"><?= $item->name ?></div>
</li>
