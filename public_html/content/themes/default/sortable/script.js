$(function () {
    // var sorted = $( "#items" ).sortable( "serialize", { key: "sort" } );
    var gup = $("#listItems").sortable({
        start: function (event, ui) {
            ui.item.toggleClass("highlight");

        },

        stop: function (event, ui) {
            ui.item.toggleClass("highlight");
            // var data = gup.sortable(  "serialize", { attribute: "data-id" }  );
            // var data = gup.sortable( "toArray" ); //default take id

            // console.log(data);

            // var data = $("#listItems").sortable('serialize', {
            //     attribute: 'data-id',//this will look up this attribute
            //     key: 'data-id',//this manually sets the key
            //     expression: /(.+)/ //expression is a RegExp allowing to determine how to split the data in key-value. In your case it's just the value

            var data = $("#listItems").sortable('toArray', {
                attribute: 'data-id',
                key: 'data-id'
            });
            // console.log(data);

            var jsonString = JSON.stringify(data, null, ' ');
            var formData = new FormData();



            formData.append('data', jsonString);
            // formData.append('menu_id', $('#sortMenuId').val());

            // console.log(formData);

            $.ajax({
                url: '/ajaxBookmarksSortItems/',
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                beforeSend: function(){

                },
                success: function(result){
                    // console.log(result);
                }
            });

        }
    });
// =========================================================
    var myArguments = {};

    function assembleData(object,arguments)
    {
        var data = $(object).sortable('toArray'); // Get array data
        var step_id = $(object).attr("id"); // Get step_id and we will use it as property name
        var arrayLength = data.length; // no need to explain

        /* Create step_id property if it does not exist */
        if(!arguments.hasOwnProperty(step_id))
        {
            arguments[step_id] = new Array();
        }

        /* Loop through all items */
        for (var i = 0; i < arrayLength; i++)
        {
            var image_id = data[i];
            /* push all image_id onto property step_id (which is an array) */
            arguments[step_id].push(image_id);
        }
        return arguments;
    }

var par = $(".bav").sortable({
    connectWith: ".bav",

        start : function( event, ui ) {
            myArguments = {}; /* Reset the array*/
        },
        /* That's fired second */
        remove : function( event, ui ) {
            /* Get array of items in the list where we removed the item */
            myArguments = assembleData(this,myArguments);
        },
        /* That's fired thrird */
        receive : function( event, ui ) {
            /* Get array of items where we added a new item */
            myArguments = assembleData(this,myArguments);
        },
        update: function(e,ui) {
            if (this === ui.item.parent()[0]) {
                /* In case the change occures in the same container */
                if (ui.sender == null) {
                    myArguments = assembleData(this,myArguments);
                }
            }
        },
        /* That's fired last */
        stop : function( event, ui ) {
            /* Send JSON to the server */
            // $("#result").html("Send JSON to the server: "+JSON.stringify(myArguments)+" ");

            // console.log(JSON.stringify(myArguments));

            var jsonString = JSON.stringify(myArguments, null);
            // var jsonString = JSON.stringify(jsonString, null, ' ');
            var formData = new FormData();
            //
            var datahtm = $("#Cats").html();
            console.log(document.getElementById('Cats').dataset.tar);
            //
            formData.append('data-inner', datahtm);
            formData.append('usId', document.getElementById('Cats').dataset.tar)
            // formData.append('menu_id', $('#sortMenuId').val());
            //
            // // console.log(formData);
            //
            $.ajax({
                url: '/ajaxHtmlInsert/',
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                beforeSend: function(){
                },
                success: function(result){
                    console.log(result);
                }
            });
            //

        }

});
// =========================================================


var gupcat = $("#catItems").sortable({


    update: function( event, ui ) {
            // var data = gup.sortable(  "serialize", { attribute: "data-id" }  );

            var data = $("#catItems").sortable('toArray', {
                attribute: 'data-id',
                key: 'data-id'
            });

            var jsonString = JSON.stringify(data, null, ' ');
            var formData = new FormData();



            formData.append('data', jsonString);
             // formData.append('menu_id', $('#sortMenuId').val());

            // console.log(formData);

            $.ajax({
                url: '/ajaxCatSortItems/',
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                beforeSend: function(){
                },
                success: function(result){
                     // console.log(result);
                }
            });

        }
    });


    $("#listItems").sortable({
        opacity: 0.6,
        cursor: 'move'
    });
    $("#catItems").sortable({
        opacity: 0.6,
        cursor: 'move'
    });


    // $( ".selector" ).sortable({
    //     connectWith: "#shopping-cart"
    // });
    //



    // .disableSelection();

    // $("#items").disableSelection();
});
