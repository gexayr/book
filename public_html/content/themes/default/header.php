<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Bookmarks</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <?php Asset::render('css'); ?>

    <!-- simple-line-icons for this template -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css">

    <!-- font-awesome-icons for this template -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.1.3/cropper.css">

    <script type="text/javascript" src="content/themes/default/js/jquery.js"></script>
    <script type="text/javascript" src="content/themes/default/sortable/ui_1.9.2_jquery-ui.js"></script>
    <script type="text/javascript" src="content/themes/default/sortable/script.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.1.3/cropper.js"></script>

    <script type="text/javascript">
    function googleTranslateElementInit() {
      new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'en,ru', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, multilanguagePage: true}, 'google_translate_element');
    }
    </script>


    <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

    <!--    <script type="text/javascript" src="http://www.pureexample.com/js/lib/jquery.ui.touch-punch.min.js"></script>-->

<!--    <script src='https://www.google.com/recaptcha/api.js'></script>-->

    <!-- Redactor CSS -->
<style>
  .skiptranslate{
    z-index: 1;
  }
  body{
      top: 0 !important;
  }
</style>

</head>

<?php
$login = User::getCookie('auth-Login');
if($login !== null){
$user = User::getUserData($login);
$user_style = json_decode($user->style_option);
//    print_r($user_style);
}else{
  if(!isset($_COOKIE['json'])){
      $user = User::getUserData("1@1.com");
      setcookie('json', $user->style_option);
  }
  $user_style = json_decode($_COOKIE['json']);
  // print_r($user_style);exit;
}
?>
<?php include 'styles.php'; ?>
<body>
      <?php
       if(isset($user_style->image)){
           echo "<div id='bg'></div>";
       }  ?>

    <header>


        <?php foreach (Customize::getInstance()->getAdminMenuItems() as $key => $item): ?>

<!--                --><?php //Lang::_e('dashboard', 'settings') ?>

        <?php endforeach; ?>

        <nav class="navbar navbar-default">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand bs-navbar" href="/"> <i class="icon-check"></i> Site</a>
        </div>



            <div class="collapse navbar-collapse" id="myNavbar">

                <ul class="nav navbar-nav">
                    <li>
                        <a class="" data-toggle="modal" href="#myModalFeedBack">
                            <i class="icon-envelope icons"></i> Feedback
                        </a>
                    </li>




            <?php if($login == null){ ?>
                <li>
                    <a data-toggle="modal" href="#myModalLogin">
                        <i class="icon-login icons"></i> Login
                    </a>
                </li>
                <li>
                    <a data-toggle="modal" href="#myModalSettings">
                        <i class="icon-settings icons"></i> Settings
                    </a>
                </li>
                <li>
                    <a data-toggle="modal" href="#myModalReg">
                        <i class="icon-login icons"></i> Registration
                    </a>
                </li>
                <? }else{ ?>


                <li>
                    <a data-toggle="modal" href="#myModalSettings">
                        <i class="icon-settings icons"></i> Settings
                    </a>
                </li>
                <li>
                    <a data-toggle="modal" href="#myModalLogOut">
                        <i class="icon-logout icons"></i> Logout
                    </a>
                </li>

                <? } ?>

                </ul>

                <div style="float:right;top:13px;position:relative" id="google_translate_element"></div>
        </div>
    </div>
    </nav>

</header>
