<?php
$login = User::getCookie('auth-Login');
//echo "<pre>";
//print_r($category);
//echo "</pre>";
?>

<?php //if(($category->parent_id) == 0) { ?>

<li class="list-group-item text-center list-group cat-item-<?= $category->id ?><?php if ($categoryId == $category->id) echo ' active'; ?><?php if (($category->parent_id) != '-') echo ' parent parent-' . $category->parent_id; ?>" data-id="<?= $category->id ?>" <?php if (($category->parent_id) != '-') echo 'parent="' . $category->parent_id . '"'; ?>
    id="<?= $category->id ?>">

    <div class="cat-list-setting">
        <div class="menu-item-event menu-item-event-remove">
            <? if ($login == null) { ?>
                <button class="button-remove" onclick="bookmark.mustReg()">
                    <i class="icon-trash icons"></i>
                </button>
            <? } else { ?>

                <? if (!isset($count)) { ?>
                    <button class="button-remove"
                            onclick="bookmark.removeCat(<?= $category->id ?>)">
                        <i class="icon-trash icons"></i>
                    </button>
                <? } else { ?>
                    <button class="button-remove"
                            onclick="bookmark.LastCat()">
                        <i class="icon-trash icons"></i>
                    </button>
                <? } ?>
            <? } ?>
        </div>

        <div class="menu-item-event menu-item-event-edit">
            <? if ($login == null) { ?>
                <a class="myBook" href="javascript:void(0)"
                   onclick="bookmark.mustReg()">
                    <i class="icon-pencil icons"></i>
                </a>
            <? } else { ?>
                <a data-toggle="modal" class="myBook"
                   href="#myCatName<?= $category->id ?>">
                    <i class="icon-pencil icons"></i>
                </a>
            <? } ?>
        </div>
    </div>
    <a class="cat-link" id="cat-item-a-<?= $category->id ?>"
       href="?category_id=<?php echo $category->id ?>">
        <?php echo $category->name ?>
    </a>

    <ul class="bav" block="<?= $category->id ?>" id="catItems-<?= $category->id ?>">

    </ul>
</li>
<?php //} ?>
