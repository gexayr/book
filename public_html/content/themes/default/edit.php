<?php $this->theme->header(); ?>
<?php if (isset($_GET['reset'])) {
    $reset = $_GET['reset'];
    $pieces = explode("+", $reset);
    $email = $pieces[0];
//    print_r($email);exit;

    $user = User::getUserData($email);
    if ($user !== null) {
        echo '<div class="alert alert-info text-center"><span> No one with this email is registered</span></div>';
        $this->theme->footer();
        return false;
    }
    $time = time();
    $time_get = $pieces[1];

    $interval = $time - $time_get;
    if($interval >= 1800) {
        echo '<div class="alert alert-danger text-center"><span>Link lifetime expired</span></div>';
        $this->theme->footer();
        return false;
    }
?>

    <div class="container">
    <div class="row">

        <br>

        <div class="col-md-12">
            <div class="well well-sm bs-well">
                <fieldset>
                    <form class="form-horizontal" method="POST" action="update/pass" onsubmit="return passConfirm()">

                        <div class="col-xs-12">
                            <div class="form-group">
                                <input type="hidden" name="email" value="<?=$email;?>">
                                <input type="password" class="form-control" id="sPass" name="password" value=""
                                       placeholder="New Password"/>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-group">
                                <input type="password" class="form-control" id="sRePass" name="confirm-password"
                                       value=""
                                       placeholder="Enter Password Again"/>
                            </div>
                        </div>
                        <div class="text-center col-xs-12">
                            <!--                        <input type="submit" class="btn btn-primary" value="Регистрация" />-->
                            <button type="submit" class="btn btn-primary">
                                Change
                            </button>
                        </div>
                </fieldset>
                </form>
            </div>
        </div>
<!--        <a href="/">-->
<!--            <button class="btn btn-primary">-->
<!---->
<!--                Продолжать-->
<!---->
<!--            </button>-->
<!--        </a>-->

    </div>
</div>
<?php } ?>
<?php $this->theme->footer(); ?>

