<?php
$login = User::getCookie('auth-Login');
?>
<?php if($login == null){ ?>

    <?php include 'not-reg.php'; ?>

<? }else{ ?>

    <?php include 'reg.php'; ?>

<? } ?>


<div id="myModalFeedBack" class="modal fade in gex-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <a class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span></a>

                <h3 class="modal-title" id="myModalLabel"> <legend class="text-center">Contact us</legend></h3>

                <!--                            <h3 class="modal-title" id="myModalLabel">Registration</h3>-->

            </div>
            <div class="modal-body">

                <div class="col-md-12">
                    <div class="well well-sm bs-well">
                        <form class="form-horizontal" action="/feed-back" method="post">
                            <fieldset>

                                <?php if($login == null){ ?>
                                    <!-- Email input-->
                                    <div class="form-group">
                                        <label class="col-md-12" for="email">Your E-mail</label>
                                        <div class="col-md-12">
                                            <input id="email" name="email" type="text" placeholder="Your email" class="form-control">
                                        </div>
                                    </div>
                                <? }else{ ?>
                                    <div class="form-group">
                                        <label class="col-md-12" for="email">Your E-mail</label>
                                        <div class="col-md-12">
                                            <input id="email" name="email" type="text" value="<?=$login;?>" class="form-control">
                                        </div>
                                    </div>


                                <? } ?>

                                <!-- Message body -->
                                <div class="form-group">
                                    <label class="col-md-12" for="message">Your message</label>
                                    <div class="col-md-12">
                                        <textarea class="form-control" id="message" name="message" placeholder="Please enter your message here..." rows="5"></textarea>
                                    </div>
                                </div>

                                <div class="form-group col-md-12">
                                    <div class="g-recaptcha" data-sitekey="6Ldd8DcUAAAAAG0ATXgRfDRNVnyGPEsAcd3JYpX8"></div>
                                </div>

                                <!-- Form actions -->
                                <div class="form-group">
                                    <div class="col-md-12 text-center">
                                        <button type="submit" class="btn btn-primary btn-lg" id="sen" disabled>Submit</button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
                <script type="text/javascript">
                    function recaptchaCallback() {
                        $('#sen').removeAttr('disabled');
                    };
                </script>

            </div>
            <!--   / .modal-body-->
            <div class="modal-footer">
                <div class="btn-group">
<!--                                                <button class="btn btn-danger" data-dismiss="modal">Cancel</button>-->
                    <!--                            <button class="btn btn-primary">Save changes</button>-->
                </div>
            </div>
            <!--   / .modal-footer-->

        </div>
        <!-- /.modal-content -->

    </div>
    <!-- /.modal-dalog -->
</div>
<!-- /.modal -->




<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->

<!--<script src="/content/themes/default/Assets/js/jquery-2.0.3.min.js"></script>-->
<script src="//cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<!--<script src="/content/themes/default/Assets/js/bootstrap.min.js"></script>-->

<script>
function setLink(){
  document.getElementById('forLet').src = "https://mini.s-shot.ru/1024x768/PNG/1024/Z100/?" + document.getElementById("BookLink").value;
  var img = new Image,
      canvas = document.getElementById("myCanvas"),
      ctx = canvas.getContext("2d"),
      src = document.getElementById("forLet").src; // insert image url here

  img.crossOrigin = "Anonymous";

  img.onload = function() {
      canvas.width = img.width;
      canvas.height = img.height;
      ctx.drawImage( img, 0, 0 );
      localStorage.setItem( "savedImageData", canvas.toDataURL("image/png") );
  }
  img.src = src;

  document.getElementById('let').value = src
}
function Cropp(){
    var cropBoxData;
    var canvasData;
    var cropper;
    var imageData;
    var image = document.getElementById("myCanvas");
    $('#imgModal').on('shown.bs.modal', function () {
      cropper = new Cropper(image, {
        autoCropArea: 0.5,
        ready: function () {
          // Strict mode: set crop box data first
          cropper.setCropBoxData(cropBoxData).setCanvasData(canvasData);
        }
      });
    }).on('hidden.bs.modal', function () {
      cropBoxData = cropper.getCropBoxData();
      canvasData = cropper.getCanvasData();
      var imagedata = cropper.getCroppedCanvas({fillColor: '#fff',width: 1920,height: 1080,minWidth: 256,minHeight: 256,maxWidth: 4096,maxHeight: 4096,imageSmoothingEnabled: true,imageSmoothingQuality: 'high'}).toDataURL();
      document.getElementById('let').value = imagedata;
      document.getElementById('forLet').src = imagedata;
      cropper.destroy();
    });
}

function setLinkE(ell){
  document.getElementById('forLet' + ell).src = "https://mini.s-shot.ru/1024x768/PNG/1024/Z100/?" + document.getElementById("link-book-" + ell).value;

  var img = new Image,
        canvas = document.getElementById("myCanvasE"),
        ctx = canvas.getContext("2d"),
        src = document.getElementById("forLet" + ell).src; // insert image url here

    img.crossOrigin = "Anonymous";
    img.onload = function() {
        canvas.width = img.width;
        canvas.height = img.height;
        ctx.drawImage( img, 0, 0 );
        localStorage.setItem( "savedImageData", canvas.toDataURL("image/png") );
    }
    img.src = src;

    document.getElementById('let' + ell).value = src;

}

function CroppE(ell){
    setLinkE(ell)
    var cropBoxData;
    var canvasData;
    var cropper;
    var imageData;
    var image = document.getElementById("myCanvasE");
    document.getElementById('nMod').setAttribute('data-target', '#myBookName' + ell)
    $('#imgModalE').on('shown.bs.modal', function () {
      cropper = new Cropper(image, {
        autoCropArea: 0.5,
        ready: function () {
          // Strict mode: set crop box data first
          cropper.setCropBoxData(cropBoxData).setCanvasData(canvasData);
        }
      });
    }).on('hidden.bs.modal', function () {
      cropBoxData = cropper.getCropBoxData();
      canvasData = cropper.getCanvasData();
      var imagedata = cropper.getCroppedCanvas({fillColor: '#fff',width: 1920,height: 1080,minWidth: 256,minHeight: 256,maxWidth: 4096,maxHeight: 4096,imageSmoothingEnabled: true,imageSmoothingQuality: 'high'}).toDataURL();
      document.getElementById('let' + ell).value = imagedata;
      document.getElementById('forLet' + ell).src = imagedata;
      cropper.destroy();
    });
}


function removeImgE(ell){
  document.getElementById('forLet' + ell).removeAttribute('src')
  document.getElementById('let' + ell).value = 'NULL'
}

function removeImg(){
  document.getElementById('forLet').removeAttribute('src')
  document.getElementById('let').value = 'NULL'
}
</script>
<?php Asset::render('js'); ?>


</body>
</html>
