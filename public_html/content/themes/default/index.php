<?php $this->theme->header(); ?>
<?php
$login = User::getCookie('auth-Login');

if ($login == null) {
    $categories = $cat_def;
//    print_r($categories);exit;
    foreach ($categories as $category) {
        $arr[] = $category->id;
    }
    $categories_id = $arr;
    if (!in_array($categoryId, $categories_id)) {
        $categoryId = min($categories_id);
        header('Location: /?category_id=' . $categoryId);
    }
} else {

    $categories = Bookmarks::getCat($login);
//    echo "<pre>";
//    print_r($categories);exit;

    foreach ($categories as $category) {
            $arr[] = $category->id;
        }
    $categories_id = $arr;

    if (!in_array($categoryId, $categories_id)) {
        $categoryId = min($categories_id);

        if($categoryId == null){
            header('Location: /logout');
            die();
        }

        header('Location: /?category_id=' . $categoryId);
    }

}

$htmldata = User::getHtmlCat($login);
?>


    <div class="container">
        <div class="row">

            <div class="panel-body col-md-12">


                <form id="custom-search-form" class="form-search form-horizontal" action="/search" method="post">
                    <input type="text" class="form-control" id="system-search" data-action="filter"
                           data-filters="#dev-table" placeholder="Search" name="search"/>
                    <button type="submit" class="btn"><i class="fa fa-search"></i></button>
                </form>


            </div>


            <div class="col-lg-4 col-md-12">
                <?php if ($login !== null) { ?>
                    <div class="text-right col-md-12 cat-list-button">
                        <a href="javascript:void(0)" class="btn btn-default" data-toggle="modal" data-target="#addCat"
                           data-whatever="@getbootstrap">
                        </a>
                    </div>
                <? } ?>

                <?php if (!empty($categories)): ?>

                    <?php
                    if (count($categories) == 1) {
                        $count = count($categories);
                    }
                    ?>

                    <div class="cat-list" id="Cats" data-tar='<?= $login ?>'>
                        <ul class="cat-list-ul bav" id="catItems">

                            <?php
                              if($htmldata != 0){
                                print_r($htmldata[0]->html_data);
                              }else {
                                foreach ($categories as $category){
                                    Theme::block('cat_item', [
                                        'category' => $category
                                    ]);
                                }
                              }
                            ?>

                        </ul>
                    </div>

                <?php else: ?>
                    <div class="empty-items">
                        <p>You do not have any categories created</p>
                    </div>
                <?php endif; ?>
            </div>


            <div class="col-lg-8 col-md-12 book-content">


                <?php if ($categoryId !== null): ?>
<!--                    <input type="hidden" id="sortMenuId" value="--><?php //echo $categoryId ?><!--"/>-->
                    <ol id="listItems" class="edit-menu bs-edit-menu col-md-12">
                        <?php if ($login !== null) { ?>


                        <button href="javascript:void(0)" class="add-item-button btn-default" data-toggle="modal" data-target="#addBook"
                           data-whatever="@getbootstrap">
                            </button>



                        <? } ?>

                        <?php foreach ($editBookmarks as $item) {
                            Theme::block('bookmark_item', [
                                'item' => $item
                            ]);
                        } ?>



                    </ol>


                <?php endif; ?>
            </div>

        </div>

    </div>



<!--add categories modal-->
    <div class="modal fade gex-modal old-modal" id="addCat" tabindex="-1" role="dialog" aria-labelledby="addMenuLabel" aria-hidden="true">
        <div class="modal-dialog bs-modal" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <a class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span></a>
                    <h4 class="modal-title" id="myModalLabel">New Category</h4>

                </div>
                <div class="modal-body">

<?php if($login == null){ ?>

    <h3 class="modal-title" id="myModalLabel">To create a category you must register</h3>

<? }else{ ?>

    <form>
                        <div class="form-group">
                            <label for="catName" class="form-control-label">Category Name</label>
                            <input type="text" class="form-control" id="catName" name="menu">
                            <input type="hidden" class="form-control" id="usLog" name="auth-login" value="<?=$login; ?>">
                        </div>

                    </form>
<? } ?>



    </div>
                <div class="modal-footer">
                    <div class="btn-group">
                        <button class="btn btn-danger" data-dismiss="modal">Cancel</button>
                        <!--                            <button class="btn btn-primary">Save changes</button>-->
<?php if($login == null){ ?>
    <button type="button" class="btn btn-primary" onclick="location.href = '/'">
        Back
    </button>
<? }else{ ?>

                    <button type="button" class="btn btn-primary" onclick="bookmark.add();">
                        Save category
                    </button>
<? } ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
<!--end add categories modal-->



<!--add bookmark modal-->
    <div class="modal fade gex-modal old-modal" id="addBook" tabindex="-1" role="dialog" aria-labelledby="addMenuLabel" aria-hidden="true">
        <div class="modal-dialog bs-modal" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <a class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span></a>
                    <h4 class="modal-title" id="myModalLabel">New Bookmark</h4>

                </div>
                <div class="modal-body">

<?php if($login == null){ ?>

    <h3 class="modal-title" id="myModalLabel">To create a category you must register</h3>

<? }else{ ?>

    <form>
                        <div class="form-group">
                            <label for="catName" class="form-control-label">Bookmark Name</label>
                            <input type="text" class="form-control" id="BookName" name="BookName">

                            <label for="catName" class="form-control-label">Bookmark Link</label>
                            <input type="text" class="form-control" id="BookLink" onchange="setLink()" name="BookLink">
                            <div class="form-group  project-form">
                                <label for="SelectCategory">Category</label>
                                <select class="form-control" id="SelectCategory" name="category">

                                    <?php foreach ($categories as $category){
                                        if($categoryId == $category->id){
                                            echo '<option value="'.$category->id.'" selected>'.$category->name.'</option>';
                                        }else{
                                            echo '<option value="'.$category->id.'">'.$category->name.'</option>';
                                        }

                                    } ?>


                                </select>
                            </div>
                        </div>
                        <input type="hidden" value="" id="let" name="let"><label for="let"><img id="forLet"  style="max-width:100px;max-height:100px;"></label>
                        <button type="button" class="btn btn-success" style="float:right;" data-target="#imgModal" data-toggle="modal" onclick="Cropp()" data-dismiss='modal'>Set The Image</button>
                        <br><button type="button" class="btn btn-warning" style="float:left;" onclick="removeImg()">Delete The Image</button>
                    </form>
<? } ?>



    </div>
                <div class="modal-footer">
                    <div class="btn-group">


                        <button class="btn btn-danger" data-dismiss="modal">Cancel</button>


                        <!--                            <button class="btn btn-primary">Save changes</button>-->
<?php if($login == null){ ?>
    <button type="button" class="btn btn-primary" onclick="location.href = '/'">
        Back
    </button>
<? }else{ ?>

                    <button type="button" class="btn btn-primary" onclick="bookmark.addItem()">
                        Save bookmark
                    </button>

<? } ?>

                    </div>
                </div>
            </div>
        </div>
    </div>

  <div class="container">


    <!-- Modal -->
    <div class="modal fade" id="imgModal" role="dialog" aria-labelledby="modalLabel" tabindex="-1">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="modalLabel">Crop</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Crop">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="img-container">
              <img id="image" hidden="true" style="max-width:100%" src="https://fengyuanchen.github.io/cropperjs/images/picture.jpg">
              <canvas style="max-width:100%" id='myCanvas'></canvas>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" data-target="#addBook" data-toggle="modal">Save</button>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="imgModalE" role="dialog" aria-labelledby="modalLabel" tabindex="-1">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="modalLabelE">Crop</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Crop">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="img-container">
              <img id="imageE" hidden="true" style="max-width:100%" src="https://fengyuanchen.github.io/cropperjs/images/picture.jpg">
              <canvas style="max-width:100%" id='myCanvasE'></canvas>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" id='nMod'  data-toggle="modal">Save</button>
          </div>
        </div>
      </div>
    </div>

  </div>

<!--end add bookmark modal-->
<?php foreach ($categories as $category){ ?>

    <div id="myCatName<?= $category->id ?>" class="myBook modal fade in gex-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <a class="btn btn-default" data-dismiss="modal"><span class="fa fa-remove" ></span></a>
                    <!--                <a class="btn btn-default" data-dismiss="modal" onclick="myReloadFunction()"><span class="glyphicon glyphicon-remove" ></span></a>-->

                    <h3 class="modal-title" id="myModalLabel">Edit Category</h3>

                </div>
                <div class="modal-body">
                    <!--                        <h4>Login</h4>-->
                    <div class="col-md-10 col-md-offset-1 well bs-login-form">
                        <i class="icon-pencil icons"></i> Name
                        <input id="name-cat-<?= $category->id ?>" class="bs-input form-control" type="text" value="<?= $category->name ?>">

                    </div>
                    <div class="form-group">

                        <div class="btn-group">
                            <button class="btn btn-danger" data-dismiss="modal">Cancel</button>
                            <button type="button" class="btn btn-primary" onclick="bookmark.updateCat(<?= $category->id ?>)">
                                Save Category
                            </button>

                        </div>
                    </div>
                </div>

            </div>
            <!-- /.modal-content -->

        </div>
        <!-- /.modal-dalog -->
    </div>

<?php } ?>

<?php foreach ($editBookmarks as $item) { ?>

    <div id="myBookName<?= $item->id ?>" class="myBook modal fade in gex-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <a class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove" ></span></a>
                    <!--                <a class="btn btn-default" data-dismiss="modal" onclick="myReloadFunction()"><span class="glyphicon glyphicon-remove" ></span></a>-->

                    <h3 class="modal-title" id="myModalLabel">Edit Bookmark</h3>

                </div>
                <div class="modal-body">
                    <div class="col-md-10 col-md-offset-1 well bs-login-form">
                        <i class="icon-pencil icons"></i> Name
                        <input id="cat-id-by-item-<?= $item->id ?>" class="bs-input form-control" type="hidden" value="<?=$categoryId;?>">
                        <input id="name-book-<?= $item->id ?>" class="bs-input form-control" type="text" value="<?= $item->name ?>">

                        <i class="icon-link icons"></i> Link
                        <input id="link-book-<?= $item->id ?>" class="bs-input form-control" type="text" value="<?= $item->link ?>">

                        <i class="icon-folder icons"></i> Category
                        <div class="form-group  project-form">
                            <select class="form-control SelectCategory" id="SelectCategory-<?= $item->id ?>" name="category">

                                <?php foreach ($categories as $category){
                                    if($categoryId == $category->id){
                                        echo '<option value="'.$category->id.'" selected>'.$category->name.'</option>';
                                    }else{
                                        echo '<option value="'.$category->id.'">'.$category->name.'</option>';
                                    }
                                } ?>


                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                      <?php if ($item->images != "NULL" && strlen($item->images) > 5) { ?>
                        <input type="hidden" value="<?= $item->images ?>" id="let<?= $item->id ?>" name="let"><label for="let"><img id="forLet<?= $item->id ?>" src="<?= $item->images ?>"  style="max-width:100px;max-height:100px;"></label>
                      <?}else{?>
                        <input type="hidden" value="" id="letE" name="let"><label for="let"><img id="forLetE"  style="max-width:100px;max-height:100px;"></label>
                        <?}?>
                      <button type="button" class="btn btn-success" style="float:right;" data-target="#imgModalE" data-toggle="modal" onclick="CroppE(<?= $item->id ?>)" data-dismiss='modal'>Edit The Image</button>
                      <br><button type="button" class="btn btn-warning" style="float:left;" onclick="removeImgE(<?= $item->id ?>)">Delete The Image</button>

                        <div class="btn-group">
                            <br>
                            <button class="btn btn-danger" data-dismiss="modal">Cancel</button>
                            <button type="button" class="btn btn-primary" onclick="bookmark.updateItem(<?= $item->id ?>)">
                                Save bookmark
                            </button>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /.modal-content -->

        </div>
        <!-- /.modal-dalog -->
    </div>

<?   } ?>


<?php $this->theme->footer(); ?>
