<?php
$login = User::getCookie('auth-Login');
?>
<div id="myModalLogOut" class="modal fade in gex-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <a class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span></a>

                <h3 class="modal-title" id="myModalLabel">Logout</h3>

                <!--                            <h3 class="modal-title" id="myModalLabel">Registration</h3>-->

            </div>
            <div class="modal-body">
                <!--                        <h4>Login</h4>-->
                <div class="col-md-12">
                    <div class="well well-sm bs-well">
                        <form class="form" action="/logout">
                            <fieldset>

                                <div class="text-center col-xs-12">
                                    <button type="submit" class="btn btn-danger center">
                                        Logout
                                    </button>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
            <!--   / .modal-body-->
            <div class="modal-footer">
                <div class="btn-group">
                    <!--                            <button class="btn btn-danger" data-dismiss="modal">Cancel</button>-->
                    <!--                            <button class="btn btn-primary">Save changes</button>-->
                </div>
            </div>
            <!--   / .modal-footer-->

        </div>
        <!-- /.modal-content -->

    </div>
    <!-- /.modal-dalog -->
</div>
<!-- /.modal -->




<?php
$user = User::getUserData($login);
$user_style = json_decode($user->style_option);
?>



<div id="myModalSettings" class="modal fade in gex-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <a class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span></a>

                <h3 class="modal-title" id="myModalLabel">My Settings</h3>



            </div>
            <div class="modal-body">
                <div class="bs-example bs-example-tabs">
                    <ul id="myTab" class="nav nav-tabs">
                        <li class="active"><a href="#styles" data-toggle="tab">Styles</a></li>
                        <li class=""><a href="#account" data-toggle="tab">Account Settings</a></li>
                    </ul>
                </div>
                <div id="myTabContent" class="tab-content">
                    <div class="tab-pane active in" id="styles">
                        <!--                            <div class="col-md-12">-->
                        <div class="well well-sm bs-well">

                            <form class="form" action="/settings" method="post">
                                <fieldset>
                                    <div class="col-sm-12">
                                        <input class="col-md-3" type="color"
                                               value="<?= $user_style->body_bg; ?>"
                                               name="body_bg"/>
                                        <span class="col-md-9">Body Background</span>
                                    </div>

                                    <div class="col-sm-12">
                                        <input class="col-md-3" type="color"
                                               value="<?= $user_style->cat_text; ?>"
                                               name="cat_text"/>
                                        <span class="col-md-9">Text Category</span>
                                    </div>

                                    <div class="col-sm-12">
                                        <input class="col-md-3" type="color"
                                               value="<?= $user_style->book_text; ?>"
                                               name="book_text"/>
                                        <span class="col-md-9">Text Bookmarks</span>
                                    </div>

                                    <div class="col-sm-12">
                                        <input class="col-md-3" type="color" value="<?= $user_style->cat_bg; ?>"
                                               name="cat_bg"/>
                                        <span class="col-md-9">Categories Background</span>
                                    </div>

                                    <div class="col-sm-12">
                                        <input class="col-md-3" type="color"
                                               value="<?= $user_style->book_bg; ?>"
                                               name="book_bg"/>
                                        <span class="col-md-9">Bookmarks Background</span>
                                    </div>

                                    <div class="col-sm-12">
                                        <input class="col-md-3" type="color"
                                               value="<?= $user_style->act_cat_text; ?>"
                                               name="act_cat_text"/>
                                        <span class="col-md-9">Active Text Category</span>
                                    </div>
                                    <div class="col-sm-12">
                                        <input class="col-md-3" type="color"
                                               value="<?= $user_style->cat_text_hover; ?>"
                                               name="cat_text_hover"/>
                                        <span class="col-md-9">Hover Text Category</span>
                                    </div>

                                    <div class="col-sm-12">
                                        <input class="col-md-3" type="color"
                                               value="<?= $user_style->act_cat_bg; ?>"
                                               name="act_cat_bg"/>
                                        <span class="col-md-9">Active Categories Background</span>
                                    </div>

                                    <div class="col-sm-12">
                                        <input class="col-md-3" type="color"
                                               value="<?= $user_style->head_bg; ?>"
                                               name="head_bg"/>
                                        <span class="col-md-9">Header Background</span>
                                    </div>

                                    <div class="col-sm-12">
                                        <input class="col-md-3" type="color"
                                               value="<?= $user_style->head_text; ?>"
                                               name="head_text"/>
                                        <span class="col-md-9">Header Text</span>
                                    </div>

                                    <div class="col-sm-12">
                                        <input class="col-md-3" type="color"
                                               value="<?= $user_style->head_text_hover; ?>"
                                               name="head_text_hover"/>
                                        <span class="col-md-9">Hover Text Header</span>
                                    </div>


                                    <div id="contrastFilterCat"
                                         style="opacity: <?= $user_style->cat_opacity; ?>;">
                                        <span>Categories opacity</span>
                                    </div>

                                    <input id="contrast-cat" type="range"
                                           value="<?= $user_style->cat_opacity; ?>" max="1"
                                           min="0.1" step="0.001" name="cat_opacity"/>


                                    <div id="contrastFilterBook"
                                         style="opacity: <?= $user_style->book_opacity; ?>;">
                                        <span>Bookmarks opacity</span>
                                    </div>

                                    <input id="contrast-book" type="range"
                                           value="<?= $user_style->book_opacity; ?>" max="1"
                                           min="0.1" step="0.001" name="book_opacity"/>


                                    <?
                                    $dir    = ROOT_DIR.'/content/themes/default/img/body';
                                    $dir2    = ROOT_DIR.'/content/themes/default/img/body/folder1';
                                    $files1 = scandir($dir);
                                    //                                            $files2 = scandir($dir, 1);
                                    array_shift($files1);
                                    array_shift($files1);



                                    foreach ($files1 as $folder) {
                                        $dir2    = ROOT_DIR.'/content/themes/default/img/body/'.$folder;
                                        $files2 = scandir($dir2);
                                        array_shift($files2);
                                        array_shift($files2);

                                        $img_fol[$folder] = $files2;
                                    }
                                    //                                                                                        echo "<pre>";
                                    //                                            print_r($files1);
                                    //                                            print_r(ROOT_DIR);
                                    //                                                                                        print_r($img_fol);
                                    //                                                                                        echo "</pre>";

                                    ?>

                                    <select id="mySelect" onchange="selectImage()">
                                        <? foreach ($files1 as $folder) { ?>
                                        <option value="<?=$folder;?>"><?=$folder;?>
                                            <? } ?>
                                            <!--                                                <option value="Audi">Audi-->
                                            <!--                                                <option value="BMW">BMW-->
                                            <!--                                                <option value="Mercedes">Mercedes-->
                                            <!--                                                <option value="Volvo">Volvo-->
                                    </select>

                                    <!--                                            <p id="images"></p>-->




                                    <section class="check">
                                        <div class="container" id="images">
                                            <? foreach ($files1 as $folder) { ?>
                                                <div id="<?=$folder;?>" style="display: <?  if($files1[0] == $folder){echo "block";}else{echo "none";}?>">
                                                    <h3 style="text-align:center;">Select any image</h3>

                                                    <?php foreach ($img_fol[$folder] as $images){ ?>

                                                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">

                                                            <div class="please-select">
                                                                <input class="image_select" type="radio" name="image" value="<?=Theme::getUrl()?>/img/body/<?=$folder;?>/<?=$images; ?>">
                                                                <a href="javascript:void(0)"><img src="<?=Theme::getUrl()?>/img/body/<?=$folder;?>/<?=$images; ?>" alt="icon" /></a>
                                                                <span class="glyphicon  check-image"></span>
                                                            </div>
                                                        </div>

                                                    <? } ?>
                                                </div>
                                            <? } ?>

                                        </div>
                                    </section>

                                    <div class="btn-group">
                                        <button class="btn btn-danger" data-dismiss="modal">Cancel</button>
                                        <button type="submit" class="btn btn-success">Save Settings</button>
                                    </div>

                                </fieldset>
                            </form>
                            <a class="back-to-def-set" href="/default-settings">Back to default settings</a>

                        </div>
                        <!--                            </div>-->
                    </div>


                    <script>

                        $(document).ready(function() {
                            $('.please-select').click(function(){
//                                     alert('dasdas');
                                $(this).find('.check-image').toggleClass('glyphicon-ok');
                                $(this).find('.image_select').prop("checked", true);
                                $(this).parent().siblings().find('.check-image').removeClass('glyphicon-ok');
                            });
                        });

                        function selectImage() {
                            var x = document.getElementById("mySelect").value;
                            $("#"+x).css("display", "block");
                            $("#"+x).siblings().css("display", "none");
//                                document.getElementById("images").innerHTML = document.getElementById( x ).innerHTML;
                        }

                    </script>

                    <div class="tab-pane fade" id="account">
                        <form class="form" method="POST" action="change/email">
                            <label class="col-md-12 text-center">Change Your E-mail</label>
                            <div class="form-group">
                                <input type="hidden" name="old-email" value="<?=$login;?>"/>
                                <input type="email" class="form-control" name="email" value="<?=$login;?>"/>
                            </div>
                            <div>
                                <button type="submit" class="btn btn-primary">
                                    Change
                                </button>
                            </div>
                        </form>
                        <br>
                        <br>
                        <form method="POST" action="change/pass" onsubmit="return passConfirm()">
                            <label class="col-md-12 text-center">Change Password</label>
                            <div class="form-group">
                                <input type="hidden" name="email" value="<?=$login;?>"/>
                                <input type="password" class="form-control" name="old-password" value=""
                                       placeholder="Old Password"/>
                            </div>

                            <!--            <label class="col-md-12 text-center">Your New Password</label>-->
                            <div class="form-group">
                                <input type="password" class="form-control" id="sPass" name="password" value=""
                                       placeholder="New Password"/>
                            </div>

                            <!--            <label class="col-md-12 text-center">Confirm New Password</label>-->
                            <div class="form-group">
                                <input type="password" class="form-control" id="sRePass" name="confirm-password"
                                       value="" placeholder="New Password Again"/>
                            </div>
                            <div>
                                <button class="btn btn-danger" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-primary">
                                    Change
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--   / .modal-body-->
            <div class="modal-footer">
                <div class="btn-group">

                </div>
            </div>
            <!--   / .modal-footer-->

        </div>
        <!-- /.modal-content -->

    </div>
    <!-- /.modal-dalog -->
</div>
<!-- /.modal -->