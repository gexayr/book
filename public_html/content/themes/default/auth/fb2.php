<?php
if (isset($_GET['code'])) {
    $result_fb = false;

    $params_fb = array(
        'client_id' => $client_id_fb,
        'redirect_uri' => $redirect_uri_fb,
        'client_secret' => $client_secret_fb,
        'code' => $_GET['code']
    );

    $tokenInfo_fb = json_decode(file_get_contents('https://graph.facebook.com/oauth/access_token' . '?' . urldecode(http_build_query($params_fb))), true);


    if (isset($tokenInfo_fb['access_token']) && isset($client_secret_fb)) {

        $ku = curl_init();

        $params_fb = 'access_token=' . $tokenInfo_fb['access_token'];


        curl_setopt($ku, CURLOPT_URL, $get_data_fb . "?" . $params_fb);
        curl_setopt($ku, CURLOPT_RETURNTRANSFER, TRUE);

        $result = curl_exec($ku);

        if (!$result) {
            exit(curl_error($ku));
        }
        $result_fb = (json_decode($result));


    }


    if ($result_fb) {
        $params['login'] = $result_fb->name;
        $params['email'] = 'https://www.facebook.com/'.$result_fb->id;
        $params['password'] = $result_fb->id;
        $params['image'] = '//graph.facebook.com/'.$result_fb->id.'/picture?type=large';
        $params['soc_akk_id'] = 'fb_'.$result_fb->id;
        User::addUs($params);
    }

}