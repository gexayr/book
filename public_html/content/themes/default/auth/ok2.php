<?php


if (isset($_GET['code'])) {
    $result_ok = false;

    $params_ok = array(
        'code' => $_GET['code'],
        'redirect_uri' => $redirect_uri_ok,
        'grant_type' => 'authorization_code',
        'client_id' => $client_id_ok,
        'client_secret' => $client_secret_ok
    );

    $url_ok = 'http://api.odnoklassniki.ru/oauth/token.do';
    $opts = array('http' =>
        array(
            'method' => 'POST',
            'header' =>"Content-type: application/x-www-form-urlencoded\r\n".
                "Accept: */*\r\n",
            'content' => http_build_query($params_ok)
        )
    );

    $tokenInfo_ok = json_decode(file_get_contents($url_ok, false, stream_context_create($opts)));


    if (isset($tokenInfo_ok->access_token) && isset($public_key_ok)) {
        $access_token_ok = "http://api.odnoklassniki.ru/fb.do";

        $url = $access_token_ok .
            '?access_token=' . $tokenInfo_ok->access_token .
            '&method=users.getCurrentUser' .
            '&application_key=' . $public_key_ok .
            '&sig=' . md5('application_key=' . $public_key_ok . 'method=users.getCurrentUser' . md5($tokenInfo_ok->access_token . $client_secret_ok));

        if (!($response = @file_get_contents($url))) {
            return false;
        }
        $result_ok = true;

        $userInfo_ok = json_decode($response);
    }


    if ($result_ok) {

        $params['login'] = $userInfo_ok->name;
        $params['email'] = 'http://www.odnoklassniki.ru/profile/'.$userInfo_ok->uid;
        $params['password'] = $userInfo_ok->uid;
        $params['image'] = $userInfo_ok->pic_3;
        $params['soc_akk_id'] = 'ok_'.$userInfo_ok->uid;
        User::addUs($params);


    }
}