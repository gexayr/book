<?php

if (isset($_GET['code'])) {
    $result_vk = false;
    $parameters = array(
        'client_id' => $client_id_vk,
        'client_secret' => $client_secret_vk,
        'code' => $_GET['code'],
        'redirect_uri' => $redirect_uri_vk
    );

    $token_vk = json_decode(file_get_contents('https://oauth.vk.com/access_token' . '?' . urldecode(http_build_query($parameters))), true);

    if (isset($token_vk['access_token'])) {
        $parameters = array(
            'uids'         => $token_vk['user_id'],
            'fields'       => 'uid,first_name,last_name,screen_name,sex,bdate,photo_big',
            'access_token' => $token_vk['access_token']
        );

        $userInfo_vk = json_decode(file_get_contents('https://api.vk.com/method/users.get' . '?' . urldecode(http_build_query($parameters))), true);
        if (isset($userInfo_vk['response'][0]['uid'])) {
            $userInfo_vk = $userInfo_vk['response'][0];
            $result_vk = true;
        }
    }
//    echo "<pre>";
//    print_r($userInfo_vk);
//    echo "</pre>";


    if ($result_vk) {

        $params['login'] = $userInfo_vk['first_name'];
        $params['email'] = 'https://vk.com/id'.$userInfo_vk['uid'];
        $params['password'] = $userInfo_vk['uid'];
        $params['image'] = $userInfo_vk['photo_big'];
        $params['soc_akk_id'] = 'vk_'.$userInfo_vk['uid'];
        User::addUs($params);

    }
}