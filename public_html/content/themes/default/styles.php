<?php

if($login !== null){
    $user = User::getUserData($login);
    $user_style = json_decode($user->style_option);
//    print_r($user_style);
}else{
    if(!isset($_COOKIE['json'])){
        $user = User::getUserData("1@1.com");
        setcookie('json', $user->style_option);
    }
    $user_style = json_decode($_COOKIE['json']);
    // print_r($user_style);exit;
}
?>

<style>
    /*Body background*/
    body{
        background-color: <?=$user_style->body_bg;?>;
    }

    /*Text Category*/
    .cat-list li a {
        color: <?=$user_style->cat_text;?>;
    }
    .cat-list li a:hover {
        color: <?=$user_style->cat_text_hover;?>;
    }

    /*Categories background*/
    /*Categories opacity*/
    .cat-list li {
        background-color: <?=$user_style->cat_bg;?>;
        opacity: <?=$user_style->cat_opacity;?>;
    }

    /*Text Bookmarks*/
    .book-items .itam-name {
        color: <?=$user_style->book_text;?>;
    }

    /*Bookmarks background*/
    /*Bookmarks opacity*/
    .bs-edit-menu li{
        background-color: <?=$user_style->book_bg;?>;
        opacity: <?=$user_style->book_opacity;?>;
    }

    header .navbar {
        background-color: <?=$user_style->head_bg;?>;
    }

    .navbar-default .navbar-nav > li > a {
        color: <?=$user_style->head_text;?>;
    }

    .navbar-default .navbar-nav > li > a:hover {
        color: <?=$user_style->head_text_hover;?>;
    }

    ul.cat-list-ul .active {
        background-color: <?=$user_style->act_cat_bg;?>;
    }

    ul.cat-list-ul .active .cat-link {
        color: <?=$user_style->act_cat_text;?>;
    }

    <?php
     if(isset($user_style->image)){
         echo "#bg{
         background-image: url(".$user_style->image.");
         background-repeat: no-repeat;
         background-position: center center;
         background-size: cover;
         height:100%;
         width:100%;
         position:fixed;
       }";
     }

    ?>

</style>